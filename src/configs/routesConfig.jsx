const routesConfig = {
  login: "/login",
  register: "/register",
  contactDetails: "/register/contact-details",
  registerPassword: "/register/password",
  forgotPassword: "/forgot-password",
  forgotConfirmation: "/forgot-password/confirmation",
  checkEmail: "/register/check-email",
  home: "/",
  profile: "/profile",
  profileInformation: "/profile/information",
  profileSecurity: "/profile/security",
  searchResults: "/searchresults",
  hotelDetails: "/hotel",

  secureBooking: "/secure-book",
  successfully: "/successfully"
};

export default routesConfig;
